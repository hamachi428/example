package model;

import java.io.Serializable;

public class User implements Serializable {
	private String id;
	private String name;
	private String pass;

	// 引数・戻り値なしのコンストラクタ
	public User() {
	}

	// コンストラクタ
	public User(String id, String name, String pass) {
		this.id = id;
		this.name = name;
		this.pass = pass;
	}

	// ゲッター
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPass() {
		return pass;
	}



}
